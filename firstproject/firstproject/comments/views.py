from django.shortcuts import render
from .models import Comment
from .forms import CommentForm
from django.http import HttpResponseRedirect
#from django.core import serializers

def PostGet(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = CommentForm(request.POST)
        # check whether it's valid:
        if form.is_valid:
            name = request.POST.get('name')
            text = request.POST.get('text')
            img = request.FILES.get('img')
            comment = Comment()
            comment.name = name
            comment.text = text
            comment.img = img
            comment.save()
            return HttpResponseRedirect('')
    all_comments = Comment.objects.all().order_by('-time')
    return render(request,"templates.html",{'form': all_comments})

#else:
    #form = CommentForm()
#return render(request, 'templates.html', {'form': form})
#def send(request):
    #comments = Comment.objects.all()
    #return render(request,"templates.html",{'form': comments})

# Create your views here.