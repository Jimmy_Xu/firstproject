from django.db import models

class Comment(models.Model):
    name = models.CharField(max_length=20)
    time = models.DateTimeField(auto_now=True)
    text = models.TextField(max_length=1000)
    img = models.ImageField(upload_to='media/')

# Create your models here.
