from django import forms
class CommentForm(forms.Form):
    name = forms.CharField(label='name',max_length=20)
    time = forms.DateTimeField()
    text = forms.CharField(label='text',max_length=1000, widget=forms.Textarea)
    img = forms.ImageField()